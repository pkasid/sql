<?php

class sql {
	
	private static function toArray ( $string ) {
		
		return explode( ',' , $string );
		
	}
	
	private static function generateDataArray ( $fields , $values ) {
		
		if ( !is_array( $fields ) || !is_array( $values ) ) throw new Exception( 'No data given.' );
		
		$fields	= array_map( 'addslashes' , $fields );
		$values	= array_map( 'addslashes' , $values );
		
		$dataArray	= array();
		
		foreach ( $fields as $key => $field ) $dataArray[ ':' . $field ] = $values[ $key ];

		return $dataArray;
		
	} 
	
	private static function dataArrayQuery ( $query , $dataArray ) {
		
		$query		= strtr( $query , $dataArray );

		if ( defined( 'sql_debugging' ) ) {

			echo "Query : " . $query . PHP_EOL;
			return;
		}
		
		return mysql_query( $query );
		
	}
	
	private static function appendDataFields ( $query , $fields , $tail = "" ) {
		
		foreach ( $fields AS $key => $field ) {
			
			$query .= $field . " = ':" . $field . "' ";
			if ( $key < ( count( $fields ) - 1 ) ) $query .= ", ";
			
		}

		$query .= " " . $tail;

		return $query;
		
	}
	
	
	public static function query ( $query , $data = null ) {
		
		if ( !$data ) $data = array();

		if ( !is_array( $data ) ) throw new Exception( 'Incompatible data.' );
		
		$dataArray	= self::generateDataArray( array_keys( $data ) , array_values( $data ) );
		
		return self::dataArrayQuery( $query , $dataArray );
		
	}
	
	public static function select ( $fields , $table , $options , $optionsData = "" ) {
		
		$table	= addslashes( $table );
		
		if ( is_string( $fields ) ) $fields = self::toArray( $fields );
		
		if ( !is_array( $fields ) ) throw new Exception( 'Incompatible data.' );
		
		$fields	= array_map( "addslashes" , $fields );
		
		$fields	= implode( "," , $fields );
		
		$query	= "SELECT " . $fields . " FROM " . $table . " " . $options . ";";
		
		if ( is_string( $optionsData ) ) $optionsData = self::toArray( $optionsData );
		
		$optionsData = self::generateDataArray( array_keys( $optionsData ) , array_values( $optionsData ) );
		
		return self::dataArrayQuery( $query , $optionsData );
		
	}

	public static function countRows ( $table , $options = "" , $optionsData = "" ) {

		return mysql_num_rows( self::select( "1" , $table , $options , $optionsData ) );

	}
	
	public static function fetch ( $fields , $table , $options , $optionsData = "" ) {
		
		$result	= self::select( $fields , $table , $options , $optionsData );
		
		$rows	= array();
		
		while ( $row = mysql_fetch_assoc( $result ) ) $rows[] = $row;
		
		return $rows;
		
	}
	
	public static function insert ( $table , $fields , $values ) {
		
		if ( is_string( $fields ) ) $fields = self::toArray( $fields );
		if ( is_string( $values ) ) $values = self::toArray( $values );
		
		if ( !is_array( $fields ) || !is_array( $values ) ) throw new Exception( 'Incompatible types' );
		
		$table	= addslashes( $table );
		
		$query	= "INSERT INTO " . $table . " SET ";
				
		$query	= self::appendDataFields( $query , $fields );

		return self::dataArrayQuery( $query , self::generateDataArray( $fields , $values ) );
		
	}
	
	public static function update ( $table , $fields , $values ) {
		
		if ( is_string( $fields ) ) $fields = self::toArray( $fields );
		if ( is_string( $values ) ) $values = self::toArray( $values );
		
		if ( !is_array( $fields ) || !is_array( $values ) ) throw new Exception( 'Incompatible types' );
		
		$table	= addslashes( $table );
		
		$query	= "UPDATE " . $table . " SET ";
				
		$query	= self::appendDataFields( $query , $fields );
		
		return self::dataArrayQuery( $query , self::generateDataArray( $fields , $values ) );
		
	}
	
	public static function delete ( $table , $options , $optionsData = "" ) {
		
		$table	= addslashes( $table );
		
		$query	= "DELETE FROM " . $table . " " . $options . ";";
		
		$optionsData = self::generateDataArray( array_keys( $optionsData ) , array_values( $optionsData ) );
		
		return self::dataArrayQuery( $query , $optionsData );
		
	}
	
}

?>
